/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly1;

import java.util.ArrayList;

/**
 *
 * @author Ouraby
 */
public class Property extends Card{
    //String name;
    String color;
    boolean utility;
    boolean wildCard;
    String otherHalfName;   //if it is a wild card, this is the name of the other half
    int setLength;
    int[] rent;
    
    public Property[] Initalize()
    {
        ArrayList<Property> PropertyList = new ArrayList<>();
        Property Railway1 = new Property();
        Railway1.name = "Railway1";
        Railway1.color = "Black";
        Railway1.utility = false;
        Railway1.wildCard = false;
        Railway1.otherHalfName = "";
        Railway1.price="m3";
        Railway1.setLength = 4;
        Railway1.rent = new int[]{1,2,3,4};
        PropertyList.add(Railway1);
        
        Property Yellow1 = new Property();
        Yellow1.name = "Yellow1";
        Yellow1.color = "Yellow";
        Yellow1.utility = false;
        Yellow1.wildCard = true;
        Yellow1.otherHalfName = "Blue1";
        Yellow1.price="m5";
        Yellow1.setLength = 3;
        Yellow1.rent = new int[]{2,4,6};
        PropertyList.add(Yellow1);
        
        Property Blue1 = new Property();
        Blue1.name = "Blue1";
        Blue1.color = "Blue";
        Blue1.utility = false;
        Blue1.wildCard = true;
        Blue1.otherHalfName = "Yellow1";
        Blue1.price="m3";
        Blue1.setLength = 2;
        Blue1.rent = new int[]{2,4};
        PropertyList.add(Blue1);
        
        //set the following for all the arraylist one time
        for(int i=0;i<PropertyList.size();i++)
        {
            PropertyList.get(i).position=position.deck;
            PropertyList.get(i).type=type.action;
        }
        Property[] A = new Property[PropertyList.size()];
        return PropertyList.toArray(A);
    }
    
    public static boolean checkCompleteSet(ArrayList<Property> set)
    {
        if(set.size() == set.get(0).rent.length)
            return true;
        else
            return false;
    }
    
    public int getRentForSet(ArrayList<Property> set)
    {
        return set.get(0).rent[set.size()-1];
    }
    
    public static int FindSet(Property card, ArrayList<ArrayList<Property>> mySets)
    {
        for(int i=0;i<mySets.size();i++)
        {
            if(card.color == mySets.get(i).get(0).color)
            {
                if(checkCompleteSet(mySets.get(i))==false)
                {
                    // check if used wild card
                    return i;
                }
            }
        }
        return -1;
    }
}
