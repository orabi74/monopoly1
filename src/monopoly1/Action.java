/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly1;

import java.util.ArrayList;

/**
 *
 * @author Ouraby
 */
public class Action extends Card{
    //String name;
    String description;
    
    public Action[] Initalize()
    {
        ArrayList<Action> ActionList = new ArrayList<>();
        Action ByPass = new Action();
        ByPass.name = "By Pass";
        ByPass.description = "Draw Extra 2 Cards";
        ByPass.price="m3";
        ActionList.add(ByPass);
        
        Action JustSayNo = new Action();
        JustSayNo.name = "Just Say No";
        JustSayNo.description = "Disable the effect of any action car";
        JustSayNo.price="m2";
        ActionList.add(JustSayNo);
        //set the following for all the arraylist one time
        for(int i=0;i<ActionList.size();i++)
        {
            ActionList.get(i).position=position.deck;
            ActionList.get(i).type=type.action;
        }
        Action[] A = new Action[ActionList.size()];
        return ActionList.toArray(A);
    }
}
