/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly1;

import java.util.ArrayList;
import java.util.Collections;
import static monopoly1.Property.*;

/**
 *
 * @author Ouraby
 */
public class Player {
    ArrayList<Card> Hand;
    ArrayList<Card> Bank;
    ArrayList<ArrayList<Property>> Sets;
    int numOfFullSets;
    public boolean checkHandLength()
    {
        if(Hand.size()<8)
            return true;
        else
            return false;
    }
    public Action PlayActionCard(int cardIndex)
    {
        Action card = (Action) Hand.remove(cardIndex);
        return card;
    }
    public void PlayPropertyCard(int cardIndex)
    {
        Property card = (Property) Hand.remove(cardIndex);
        int sameSet = FindSet(card,this.Sets);
        if(sameSet != -1)   // set exists
        {
            Sets.get(sameSet).add(card);
            if(checkCompleteSet(Sets.get(sameSet)) == true)
                this.numOfFullSets++;
            if(this.numOfFullSets == 3)
                ;   // Game Over
        }
        else    // new set
        {
            ArrayList<Property> newSet = new ArrayList<>();
            newSet.add(card);
            this.Sets.add(newSet);
        }  
    }
    public void PlayToBank(int cardIndex)
    {
        Card card = Hand.remove(cardIndex);
        this.Bank.add(card);
    }
    public ArrayList<Card> PayfromBank(int[] indexes)
    {
        ArrayList<Card> toPay = new ArrayList<>();
        for(int i=0;i<indexes.length;i++)
        {
            Card tmp = this.Bank.get(indexes[i]);
            toPay.add(tmp);
            this.Bank.set(indexes[i], null);
        }
        this.Bank.removeAll(Collections.singleton(null)); 
        return toPay;
    }
}
