/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author Ouraby
 */
enum cardType{
    property,
    money,
    action
};
enum cardPosition{
    deck,
    inhand,
    bank,
    faceup,
    center
};

public class Card {
    cardType type;
    String price;
    cardPosition position;
    public String name;
    
    public ArrayList<Card> createDeck()
    {
        Money M = new Money();
        Money[] MoneyCards = M.Initalize(10);
        Action A = new Action();
        Action[] ActionCards = A.Initalize();
        Property P = new Property();
        Property[] PropertyCards = P.Initalize();
        ArrayList<Card> Deck = new ArrayList<>();
        Deck.addAll(Arrays.asList(MoneyCards));
        Deck.addAll(Arrays.asList(ActionCards));
        Deck.addAll(Arrays.asList(PropertyCards));
        
        return Deck;
        
    }
    public void DisplayDeck(ArrayList<Card> Deck)
    {
        Deck.stream().forEach((c) -> {
            System.out.println(c.name);
        });
    }
    public ArrayList<Card> Shuffle(ArrayList<Card> Deck)
    {
        long seed = System.nanoTime();
        Collections.shuffle(Deck, new Random(seed));
        return Deck;
    }
    public Deck_Card_Bundle drawCard(ArrayList<Card> Deck,int numOfCards)
    {
        Deck_Card_Bundle DC = new Deck_Card_Bundle();
        for(int i=0;i<numOfCards;i++)
        {
           DC.C.add(Deck.get(0));
           Deck.remove(0); 
        }
        DC.Bundle=Deck;
        return DC;
    }
}

