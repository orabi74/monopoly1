/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly1;

/**
 *
 * @author Ouraby
 */
enum moneyCard{
    m1,
    m2,
    m3,
    m5,
    m10
};
public class Money extends Card{
    moneyCard value;
    
    public Money[] Initalize(int length)
    {
        Money[] moneyCards = new Money[length];
        for(int i=0;i<length;i++)
        {
            moneyCards[i]=new Money();
            moneyCards[i].type=cardType.money;
            moneyCards[i].position=position.deck;
            moneyCards[i].value=moneyCard.m1;
            moneyCards[i].price=moneyCard.m1.name();
            moneyCards[i].name=moneyCard.m1.name();
        }
        return moneyCards;
    }
    
}
