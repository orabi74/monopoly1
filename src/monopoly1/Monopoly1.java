/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly1;

import java.util.ArrayList;

/**
 *
 * @author Ouraby
 */
public class Monopoly1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Card C = new Card();
        ArrayList<Card> withdrawnCards;
        Deck_Card_Bundle tmp;
        ArrayList<Card> Deck = C.createDeck();
        ArrayList<Card> UsedCards = new ArrayList<>();
        C.DisplayDeck(Deck);
        System.out.println("---");
        Deck = C.Shuffle(Deck);
        C.DisplayDeck(Deck);
        System.out.println("---");
        tmp = C.drawCard(Deck,3);
        Deck = tmp.getBundle();
        withdrawnCards = tmp.getC();
        UsedCards.addAll(withdrawnCards);
        C.DisplayDeck(withdrawnCards);
        //System.out.println("Card: "+withdrawnCard.name);
        System.out.println("---Current Deck");
        C.DisplayDeck(Deck);
        System.out.println("---Used Deck");
        C.DisplayDeck(UsedCards);
        System.out.println("---");
        tmp = C.drawCard(Deck,2);
        Deck = tmp.getBundle();
        withdrawnCards = tmp.getC();
        UsedCards.addAll(withdrawnCards);
        C.DisplayDeck(withdrawnCards);
        System.out.println("---Current Deck");
        C.DisplayDeck(Deck);
        System.out.println("---Used Deck");
        C.DisplayDeck(UsedCards);
    }
    
}
